# Mongrator
A MongoDB migration / refactoring tool, sort of like Liquibase

## Installation
Clone this repository onto your local drive, then navigate into the directory and execute `npm install -g ./`

## Usage
`mongrator` options:

    --config="/path/to/config_file.json"
        The path to the configuration JSON file

    --schemasDir="/path/to/schemas/"
        The path to your schemas directory (if specified, will override schemasDir in config)

    --master="/path/to/db-changelog-master.json"
        The path to the master changelog JSON file (if specified, will override master in config)

## Specifications
TODO: specifications
see example db-changelog files
