var mongoose = require('mongoose');
var argv = require('optimist').argv;
var jf = require('jsonfile');

// This is for saving the changelog stuff in the DB, to be done later
//require('./log_entries.js');
//var Changelog = mongoose.model('Changelog');

console.log('args:', argv);

master = { };
config = { };
schemas = "";
ref_table = { };

// Get config file path from --config option
if (argv['config'] || undefined) {
    config = jf.readFileSync(argv.config);

    console.log("Using config: ", config);

    // Connect to mongoose using the option sspecified in the config file
    mongoose.connect(config.uri + '/' + config.database);
} else {
    // TODO change this
    console.log('Error: no config file!');
    throw('ass');
}

// Get schemas path from config file or from --schemasDir option
if (schemas = (argv['schemasDir'] || config.schemasDir || undefined))
{
    require(process.cwd() + '/' + schemas);
} else {
    // TODO change this
    console.log('Error: please specify schemasDir in config file or with the option --schemasDir={path/to/schemas/dir}');
    throw('dick');
}

/**
 * This recurses through the given object and checks for the key 'xref'
 * If the key is found, the object itself is replaced in its parent with the id value given from the key
 * This is to resolve reference names from the json files into ObjectID's that Mongoose can use to link objects
 * @param obj
 * @param parent
 * @param pkey
 */
var resolve_references = function(obj, parent, pkey) {
    try {
        Object.keys(obj).forEach(function (key) {
            if (obj.hasOwnProperty(key)) {

                // Check for "xref" property
                if (key == 'xref') {
                    var ref = obj[key]; // Get the reference string
                    if (!ref_table[ref]) // Check the table
                        throw('Invalid reference!');
                    parent[pkey] = ref_table[ref]; // Replace this object in the parent with the ID in the ref table
                } else {
                    // Recursive call to continue searching the document
                    resolve_references(obj[key], obj, key);
                }

            }
        });
    } catch(e) {
        // This catches the error thrown when you try to use Object.keys on a non-object
        return;
    }
};

// Scans through the changelogs and push all the changes into the master_changeset
master_changeset = [];
if (master = (argv['master'] || config.master || undefined))
{
    // Read JSON into object
    master = jf.readFileSync(master);

    // Iterate over changelogs
    master.changelogs.forEach(function(changelog) {
        var log = jf.readFileSync(changelog.filepath);

        // Iterate over changesets
        for (var i in log.changesets) {
            var changeset = log.changesets[i];

            // Iterate over changes
            for (var j in changeset.changes) {
                master_changeset.push(changeset.changes[j]);
            }
        }
    });

    // Sort by change id's
    master_changeset.sort(function(a, b) {
        return a.id- b.id;
    });
    complete();
}

/**
 * This function actually executes the DB changes in order
 *
 * It accomplishes this by popping changes off the master_changeset and executing them, using this function as the
 * callback for each of the changes. This ensures the changes will occur in the order they appear in master_changeset.
 */
function complete() {
    // If we're out of changes, finish up
    if (!master_changeset.length) {
        console.log(Math.random() * 10 > 2 ? "Mongration complete!" : "MONGRATION, YOU DONE IT!");
        mongoose.disconnect();
        return;
    }
    // Pop a change off the master_changeset
    var change = master_changeset.shift();
    var model    = mongoose.model(change.schema);

    console.log(change);

    // Check what kind of change it is
    switch (change['type'].toLowerCase()) {
        case 'insert':
            console.log('insert document');
            var document = new model();

            // Check for "xref" properties and resolve the references from ref_table
            resolve_references(change.values, null, null);

            // Copy keys from values into new document
            for (var key in change.values) {
                document[key] = change.values[key];
            }

            // Add reference to new object in ref_table if necessary
            if (change['ref'] || undefined) {
                ref_table[change['ref']] = document._id;
            }

            // Save new document to the DB, with this function as the callback
            document.save(complete);
            break;
        case 'update':
            console.log("update document");
            //TODO Can we wrap this in the same code ref/xref code as insert?
            // Check for "xref" properties and resolve the references from ref_table
            resolve_references(change.values, null, null);

            model.update(change['where'], {$set: change['values']}, {}, complete);
            break;
        case 'remove':
            console.log('remove document');
            model.find(change["where"]).remove(complete);
            break;
        case 'dropcollection':
            console.log('drop collection');

            // Drop specified collection with this function as callback
            mongoose.connection.collections[change.collection].drop(complete);
            break;
        case 'createcollection':
            console.log("create collection");
            //TODO
            break;
        case 'createindex':
            console.log("create index");
            //TODO
            break;
    }

    function updateDocs(docs, values){
        for(var doc in docs) {
            for (var key in values) {
                doc[key] = values[key];
            }

        }
        return docs;
    }
}

