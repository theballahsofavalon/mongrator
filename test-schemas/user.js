var mongodb = require('mongodb');
var mongoose = require('mongoose');
var passportLocalMongoose = require('passport-local-mongoose');

var Schema = mongoose.Schema;
var ObjectID = mongoose.Types.ObjectID;

var userSchema = new Schema({
	email: String,
	roles: [ObjectID],
	user_name: String,
	user_tag: String
});

// this plugin automatically adds username, hash, and salt fields to the schema
userSchema.plugin(passportLocalMongoose);

mongoose.model('User', userSchema);