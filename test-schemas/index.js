/* Reads all the other .js files in this directory and puts their exports in this file's exports */
/* Note that index.js is loaded when you require() a folder */
require('fs').readdirSync(__dirname + '/').forEach(function(file) {
	if (file.match(/.+\.js/g) !== null && file !== 'index.js') {
		var name = file.replace('.js', '');
		require('./' + file);
	}
});