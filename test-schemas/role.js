var mongodb = require('mongodb');
var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var roleSchema = new Schema({
    name: String
});

mongoose.model('Role', roleSchema);